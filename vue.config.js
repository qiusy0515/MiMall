const path = require('path')
// 代理方式：
module.exports={
    devServer:{
        host:'localhost',
        port:8080,
        proxy:{
            '/api':{
                target:'http://mall-pre.springboot.cn',
                changeOrigin:true,
                pathRewrite:{
                    '/api':''
                }
            }
        }
    },
    chainWebpack: config => {
      const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
      types.forEach(type => addStyleResource(config.module.rule('less').oneOf(type)))
      // 删除预加载的路由
      config.plugins.delete('prefetch')
    },
    // eslint:
    lintOnSave:false,

}

function addStyleResource (rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        // 在此处添加less的mixin和变量
        path.resolve(__dirname, './src/assets/less/less_reset.less'),
        path.resolve(__dirname, './src/assets/less/config.less')
      ],
    })
}
