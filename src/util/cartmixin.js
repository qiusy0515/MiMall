// 定义公共的属性和方法
import {
    Message
} from "element-ui";
export default {
    name: 'cartmixin',
    data() {
        return {
            // 购物车商品列表:
            cartlist: [],
            // 购物车总金额
            cartTotalPrice: 0,
        }
    },
    methods: {
        // 删除购物车:
        delProduct(item) {
            this.axios.delete(`/carts/${item.productId}`).then(res => {
                Message.success("删除购物车商品成功");
                this.renderData(res);
            });
        },
    },
}