let baseURL;
// 获取nodejs中的环境变量
switch (process.env.NODE_ENV) {
    case 'development':
        baseURL='http://dev-mall-pre.springboot.cn/api';
        break;
    case 'test':
        baseURL='http://test-mall-pre.springboot.cn/api';
        break;
    case 'prod':
        baseURL='http://mall-mall-pre.springboot.cn/api';
        break;
    default:
        baseURL='http://mall-mall-pre.springboot.cn/api';
        break
}

export default{
    baseURL
}