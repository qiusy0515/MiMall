 // 如果不加./一般判断为是vue.js的插件，会优先从node_module中查找
 // 如果是 ./开头的则会从自己目录查找
 import Vue from 'vue'
 import App from './App.vue'
 import router from './router'
 import axios from 'axios'
 import VueAxios from 'vue-axios'
 import VueLazyLoad from 'vue-lazyload'
 import VueCookie from 'vue-cookie'
 import {
   Message
 } from 'element-ui';
 import 'element-ui/lib/theme-chalk/index.css';
 import Vuex from 'vuex'
 import store from './store'


 // Vue.component(Message.name, Message);
 // 不加./会默认为是个插件.自己的文件需要加./
 // import env from './env'
 // mock开关
 const mock = false;
 if (mock) {
   require('./mock/api')
 }
 // 根据前端的跨域方式调整： /a/b :/api/a/b => /a/b
 // axios.defaults.baseURL=env.baseURL
 axios.defaults.baseURL = '/api';
 // 超出请求时间设置：
 axios.defaults.timeout = 8000
 // axios接口错误拦截器：
 axios.interceptors.response.use(function (response) {
   // 获取到接口的返回值
   let res = response.data;
   let path = location.hash;

   // 只有是0的时候才是成功：
   if (res.status == 0) {
     // 成功返回数据：
     return res.data;

     // 登录错误的报错码：
   } else if (res.status == 10) {
     // 跳转回login并抛出异常：
     if (path != '#/index') {
       window.location.href = '/#/login';
     }
     return Promise.reject(res)
   } else {
     // 弹错其他错误:
     Message.warning(res.msg)
     return Promise.reject(res);
   }
 }, (error) => {
   let res = error.response;
   Message.error(res.data.message);
   return Promise.reject(error);
 })



 Vue.use(VueAxios, axios);
 Vue.use(VueCookie);
 Vue.use(Vuex);
 Vue.use(VueLazyLoad, {
   loading: '/imgs/loading-svg/loading-bubbles.svg'
 });
 // Vue.use(Message);

 // 通过原型的方式扩展对象,绑定message:
 Vue.prototype.$message = Message;
 Vue.config.productionTip = false

 new Vue({
   router,
   store,
   render: h => h(App),
 }).$mount('#app')


 // import './../src/assets/less/less_reset.less';