import Vue from 'vue'
import Router from 'vue-router'
import Home from './pages/home'
import Index from './pages/index'
import VueRouter from 'vue-router';

// import Login from './pages/login'
// import Product from './pages/product'
// import Detail from './pages/detail'
// import Cart from './pages/cart'
// import Order from './pages/order'
// import OrderConfirm from './pages/orderConfirm'
// import OrderList from './pages/orderList'
// import OrderPay from './pages/orderPay'
// import AliPay from './pages/alipay'


Vue.use(Router);

export default new Router({
    // 配置路由列表
    routes:[
        {
            path:'/',
            name:'home',
            component:Home,
            // 默认重定向到index页面
            redirect:'/index',
            // 子路由
            children:[
                {
                    path:'/index',
                    name:'index',
                    component:Index,                    
                },                {
                    path:'/product/:id',
                    name:'product',
                    component:()=>import('./pages/product.vue')                 
                },                {
                    path:'/detail/:id',
                    name:'detail',
                    component:()=>import('./pages/detail.vue')                  
                }
            ]
        },
        {
            path:'/login',
            name:'login',
            // 使用es7import语法结合@babel/plugin-syntax-dynamic-import来异步加载路由
            component:()=>import('./pages/login.vue')    
        },
        {
            path:'/cart',
            name:'cart',
            component:()=>import('./pages/cart.vue')    
        },
        {
            path:'/register',
            name:'register',
            component:()=>import('./pages/register.vue')    
        },
        // 根组件:
        {
            path:'/order',
            name:'order',
            component:()=>import('./pages/order.vue'), 
            children:[
                {
                    path:'list',
                    name:'order-list',
                    component:()=>import('./pages/orderList.vue')    
                },
                {
                    path:'confirm',
                    name:'order-confirm',
                    component:()=>import('./pages/orderConfirm.vue')    
                },
                {
                    path:'pay',
                    name:'order-pay',
                    component:()=>import('./pages/orderPay.vue')    
                },
                {
                    path:'alipay',
                    name:'alipay',
                    component:()=>import('./pages/alipay.vue')    
                }
            ]            
        }
    ]
});

