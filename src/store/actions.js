export default {
    saveUserName(context,username){
        // 派发事件到mutation修改state:
        context.commit('saveUserName',username)
    },
    saveCartCount(context,count){
        context.commit('saveCartCount',count)
    }
}