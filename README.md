## Vue仿小米商城

#### 演示地址:[http://47.110.13.193:8000/](http://47.110.13.193:8000/#/index)

测试账号admin;测试密码admin

#### 后端数据接口
http://mall-pre.springboot.cn

#### 使用技术

- vue: 2.6.10
- vue-router:3.1.5
- vuex:3.1.2
- axios:0.19.2
- element-ui:2.13.0
- less:3.10.3
- vue-cookie:1.1.4
- vue-lazyload:1.3.3
- vue-awesome-swiper:3.1.3
- vue-infinite-scroll:2.0.2

#### 使用说明

```
npm run install  ---安装项目依赖
```
```
npm run serve    ---运行开发环境
```
```
npm run build    ---打包生产环境
```


#### 演示界面

 **首页** 
![首页](https://images.gitee.com/uploads/images/2020/0410/120019_214a90f5_5066724.png "QQ截图20200410115525.png")

 **登录页面** 
![登录](https://images.gitee.com/uploads/images/2020/0417/115407_608b16eb_5066724.png "login.png")

 **注册页面** 
![注册](https://images.gitee.com/uploads/images/2020/0410/120119_6d75a842_5066724.png "QQ截图20200410115546.png")

 **导航购物车** 
![登录后](https://images.gitee.com/uploads/images/2020/0410/120143_78e1bae5_5066724.png "QQ截图20200410115852.png")

 **商品详情** 
![商品详情](https://images.gitee.com/uploads/images/2020/0410/120213_f8b01c08_5066724.png "QQ截图20200410120048.png")

 **购物车** 
![购物车](https://images.gitee.com/uploads/images/2020/0410/120241_6f717ca8_5066724.png "QQ截图20200410115904.png")

 **订单确认** 
![购物车地址](https://images.gitee.com/uploads/images/2020/0410/120303_f1cf7fd0_5066724.png "QQ截图20200410115948.png")

 **订单列表** 
![订单列表](https://images.gitee.com/uploads/images/2020/0417/115230_36ed98a3_5066724.png "QQ截图20200417115122.png")

 **微信支付** 
![微信支付](https://images.gitee.com/uploads/images/2020/0410/120417_625f5b58_5066724.png "QQ截图20200410120003.png")

 **支付宝支付** 
![支付宝支付](https://images.gitee.com/uploads/images/2020/0410/120503_a900dc70_5066724.png "QQ截图20200410120549.png")

#### 特别鸣谢~
慕课网 [Vue全家桶实战 从零独立开发企业级电商系统](https://coding.imooc.com/class/397.html)

